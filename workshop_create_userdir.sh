#!/bin/bash

ROOT_DIR="/data"

APP_USER="${APP_USER:-default}"
APP_SLEEP_TIME="${APP_SLEEP_TIME:-36000}"

if [ "${#}" -eq 1 ]
then
    APP_USER="${1}"
fi


if ! [ -d "${ROOT_DIR}/${APP_USER}" ]
then
    mkdir "${ROOT_DIR}/${APP_USER}"
    (
        cat <<EOS
#!/bin/bash

export KUBECONFIG=${ROOT_DIR}/${APP_USER}/kubeconfig

EOS
    ) > "${ROOT_DIR}/${APP_USER}/env.sh"
fi

sleep "${APP_SLEEP_TIME}"

