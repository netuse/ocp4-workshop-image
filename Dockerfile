FROM image-registry.openshift-image-registry.svc:5000/openshift/cli
RUN dnf install git -y && dnf clean all
ADD workshop_create_userdir.sh /usr/local/sbin/workshop_create_userdir.sh
